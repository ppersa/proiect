import { environment } from "src/environments/environment";
import { Entity } from "./entity";

/*
{
            "aliases": [],
            "banner": "/banners/graphical/70566-g2.jpg",
            "firstAired": "1995-1-1",
            "id": 70566,
            "image": "/banners/posters/70566-1.jpg",
            "network": "Swearnet",
            "overview": "Ricky and Julian are two guys whose lives were shaped by their experiences growing up in the Trailer Park. Their childhood was typical of most trailer park kids - stealing, fighting, smoking, drinking, scamming and listening to Van Halen. The Boys have had their share of trouble with the law and spend a large portion of their adult lives behind bars.",
            "poster": "/banners/posters/70566-1.jpg",
            "seriesName": "Trailer Park Boys",
            "slug": "trailer-park-boys",
            "status": "Ended"
        }
*/
export class SearchResult extends Entity {

    id: number;
    seriesName: string;
    banner: string;
    firstAired: string;
    network: string;
    overview: string;

    getBannerUrl(): string {
        if(!this.banner?.length)
            return '';
        return `${environment.artworkUrl}${this.banner}`
    }

}