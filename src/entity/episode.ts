/*
 {
            "id": 261862,
            "airedSeason": 1,
            "airedSeasonID": 13782,
            "airedEpisodeNumber": 1,
            "episodeName": "Hi Diddle Riddle",
            "firstAired": "1966-01-12",
            "guestStars": [
                "Allen Jaffe",
                "Ben Astar",
                "Damian O'Flynn",
                "Jack Barry",
                "Jill St. John",
                "Michael Fox",
                "Richard Reeves"
            ],
            "directors": [
                "Robert Butler"
            ],
            "writers": [
                "Lorenzo Semple Jr."
            ],
            "overview": "The Riddler leaves a clue at a Moldavian reception at the Gotham City World’s Fair. Batman and Robin are summoned and are on the villain’s trail. But he tricks them; the heroes think he’s committing a robbery with a handgun. In reality, the gun is a cigarette lighter (the answer to one of the Riddler’s riddles). Now, the villain is suing Batman, where he will be forced to reveal his true identity in court. The heroes, still convinced the Riddler is planning a major crime, travel to a discotheque. There, Batman is drugged by Molly, one of the Riddler’s confederates, while Robin is kidnapped. Robin appears to be in great danger as the episode ends.",
            "language": {
                "episodeName": "en",
                "overview": "en"
            },
            "productionCode": "6028-1",
            "showUrl": "",
            "lastUpdated": 1563534563,
            "dvdDiscid": "",
            "dvdSeason": 1,
            "dvdEpisodeNumber": 1,
            "dvdChapter": null,
            "absoluteNumber": 1,
            "filename": "episodes/77871/261862.jpg",
            "seriesId": 77871,
            "lastUpdatedBy": 1,
            "airsAfterSeason": null,
            "airsBeforeSeason": null,
            "airsBeforeEpisode": null,
            "imdbId": "tt0519475",
            "contentRating": "TV-PG",
            "thumbAuthor": 1,
            "thumbAdded": "2019-11-13 10:04:36",
            "thumbWidth": "640",
            "thumbHeight": "360",
            "siteRating": 8,
            "siteRatingCount": 319,
            "isMovie": 0
        },
*/

import { environment } from "src/environments/environment";
import { Entity } from "./entity";

export class Episode extends Entity{

    id: number;
    airedSeason: number;
    airedEpisodeNumber: number;
    episodeName: string;
    firstAired: Date;
    siteRating: number;
    directors: string[];
    filename: string;
    overview: string;

    getImageUrl() {
        if(!this.filename?.length)
            return '';
        return `${environment.artworkUrl}/banners/${this.filename}`
    }

}