import { environment } from "src/environments/environment";
import { Entity } from "./entity";

/*
{
    "data": {
        "id": 77871,
        "seriesId": "713",
        "seriesName": "Batman",
        "aliases": [],
        "season": "3",
        "poster": "posters/77871-7.jpg",
        "banner": "graphical/77871-g.jpg",
        "fanart": "fanart/original/77871-10.jpg",
        "status": "Ended",
        "firstAired": "1966-01-12",
        "network": "ABC (US)",
        "networkId": "5",
        "runtime": "25",
        "language": "en",
        "genre": [
            "Action",
            "Adventure",
            "Comedy",
            "Crime",
            "Family",
            "Fantasy",
            "Science Fiction",
            "Suspense"
        ],
        "overview": "Wealthy entrepreneur Bruce Wayne and his ward Dick Grayson lead a double life: they are actually crime fighting duo Batman and Robin. \r\nA secret Batpole in the Wayne mansion leads to the Batcave, where Police Commissioner Gordon often calls with the latest emergency threatening Gotham City. Racing the the scene of the crime in the Batmobile, Batman and Robin must (with the help of their trusty Bat-utility-belt) thwart the efforts of a variety of master criminals, including The Riddler, The Joker, Catwoman, and The Penguin.",
        "lastUpdated": 1611872839,
        "airsDayOfWeek": "Daily",
        "airsTime": "4:00 PM",
        "rating": "TV-PG",
        "imdbId": "tt0059968",
        "zap2itId": "EP00000490",
        "added": "2008-02-04 00:00:00",
        "addedBy": 1,
        "siteRating": 8.3,
        "siteRatingCount": 1266,
        "slug": "batman"
    }
}
*/
export class Series extends Entity{

    id: number;
    seriesName: string;
    banner: string;
    poster: string;
    status: string;
    airsDayOfWeek: string;
    airsTime: string;
    genre: string[];
    siteRating: number;
    overview: string;
    season: number;

    getBannerUrl(): string {
        if(!this.banner?.length)
            return '';
        return `${environment.artworkUrl}${this.banner}`
    }

    getPosterUrl(): string {
        if(!this.poster?.length)
            return '';
        return `${environment.artworkUrl}/banners/${this.poster}`
    }

}