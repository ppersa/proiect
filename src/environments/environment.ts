export const environment = {
  production: false,
  proxyUrl: 'http://localhost:5000/',
  apiUrl: 'https://api.thetvdb.com/',
  apiKey: 'bce69c15bd31ea9144c6910475885692',
  artworkUrl: 'https://artworks.thetvdb.com'
};
