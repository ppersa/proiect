import { serializeNodes } from '@angular/compiler/src/i18n/digest';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { SearchResult } from 'src/entity/search-result';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {

  searchName: string = '';
  resultsSubscription: Subscription;
  results: SearchResult[]; 
  loadingResults = true;
  resultsFound = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.loadingResults = true;
    this.activatedRoute.queryParams.subscribe(params => {
      this.searchName = params['name'];
      this.resultsSubscription = this.dataService.getSearchResults(this.searchName).subscribe(
        data => {
          this.results = data.data.map( (x: any) => new SearchResult(x));
          this.loadingResults = false;
        },
        error => {
          this.resultsFound = false;
        });
    });
  }

}
