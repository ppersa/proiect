import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchResult } from 'src/entity/search-result';

@Component({
  selector: 'app-search-details',
  templateUrl: './search-details.component.html',
  styleUrls: ['./search-details.component.scss']
})
export class SearchDetailsComponent implements OnInit {
  @Input() series: SearchResult;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirect() {
    this.router.navigate(['/series/' + this.series.id]);
  }

}
