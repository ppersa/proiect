import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'movie';

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute) {
    auth.init();
  }

  ngOnInit() {
  }

}
