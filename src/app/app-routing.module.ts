import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchHomeComponent } from './search/search-home/search-home.component';
import { SearchResultsComponent } from './search/search-results/search-results.component';
import { SeriesComponent } from './series/series/series.component';

const routes: Routes = [
  { path: 'home', component: SearchHomeComponent },
  { path: 'search', component: SearchResultsComponent },
  { path: 'series/:id', component: SeriesComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
