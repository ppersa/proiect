import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Series } from 'src/entity/series';

@Component({
  selector: 'app-series-details',
  templateUrl: './series-details.component.html',
  styleUrls: ['./series-details.component.scss']
})
export class SeriesDetailsComponent implements OnInit {
  @Input() series: Series;
  genre: string = '';

  constructor(
  ) {
  }

  ngOnInit(): void {
    this.genre = this.series.genre.join(', ');
  }

}
