import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Episode } from 'src/entity/episode';
import { Series } from 'src/entity/series';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.scss']
})
export class SeriesComponent implements OnInit {
  
  seriesId: number;
  series: Series;
  episodes: Episode[] = [];
  loadingSeries = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.seriesId = params['id'];
      this.getSeries();
    });
  }

  getSeries() {
    this.loadingSeries = true;
    this.dataService.getSeriesDetails(this.seriesId).subscribe(data => {
      this.series = new Series(data.data);
      this.loadingSeries = false;
    });
  }

}
