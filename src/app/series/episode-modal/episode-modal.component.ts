import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Episode } from 'src/entity/episode';

@Component({
  selector: 'modal-content',
  templateUrl: './episode-modal.component.html',
  styleUrls: ['./episode-modal.component.scss']
})
export class EpisodeModalComponent implements OnInit {

  episode: Episode;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit(): void {
  }

}
