import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Episode } from 'src/entity/episode';
import { DataService } from 'src/services/data.service';
import { EpisodeModalComponent } from '../episode-modal/episode-modal.component';

@Component({
  selector: 'app-series-episodes',
  templateUrl: './series-episodes.component.html',
  styleUrls: ['./series-episodes.component.scss']
})
export class SeriesEpisodesComponent implements OnInit {
  @Input() seriesId: number;
  @Input() seasons: number;
  episodes: Episode[] = [];
  bsModalRef: BsModalRef;
  loadedEpisodes: Array<boolean>;
  noEpisodes = false;

  constructor(private modalService: BsModalService,
    private dataService: DataService) { }

  ngOnInit(): void {
    this.loadedEpisodes = Array(this.seasons).fill(false);
    
  }

  loadEpisodes(season: number) {
    this.noEpisodes = false;
    this.dataService.getEpisodesBySeason(this.seriesId, season).subscribe(
      data => {
        for(let episode of data.data) {
          this.episodes.push(new Episode(episode));
          this.loadedEpisodes[season] = true;
        }},
      error => {
        this.noEpisodes = true;
      });
  }

  seasonsArray() {
    let seasonsArray: number[] = [];
    for (let season = 1; season <= this.seasons; season++) {
      seasonsArray.push(season);
    }
    return seasonsArray;
  }

  seasonEpisodes(season: number){
    let episodesArray: Episode[] = [];
    for (let episode of this.episodes) {
      if (episode.airedSeason === season) {
        episodesArray.push(episode);
      }
    }
    return episodesArray;
  }

  showDetails(episode: Episode) {
    const initialState = {
      episode: episode
    };
    this.bsModalRef = this.modalService.show(EpisodeModalComponent, {animated: true, initialState} );
  }

  expandSeason($event: boolean, season: number) {
    if ($event === true) {
      if(!this.loadedEpisodes[season]) {
        this.loadEpisodes(season);
      }
    }
  }

}
