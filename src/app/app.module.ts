import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchHomeComponent } from './search/search-home/search-home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestHttpInterceptor } from 'src/services/request-http-interceptor';
import { SearchBarComponent } from './search/search-bar/search-bar.component';
import { SearchResultsComponent } from './search/search-results/search-results.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchDetailsComponent } from './search/search-details/search-details.component';
import { SeriesDetailsComponent } from './series/series-details/series-details.component';
import { SeriesComponent } from './series/series/series.component';
import { SeriesEpisodesComponent } from './series/series-episodes/series-episodes.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { RatingModule } from 'ngx-bootstrap/rating';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { EpisodeModalComponent } from './series/episode-modal/episode-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchHomeComponent,
    SearchBarComponent,
    SearchResultsComponent,
    SearchDetailsComponent,
    SeriesDetailsComponent,
    SeriesComponent,
    SeriesEpisodesComponent,
    EpisodeModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    RatingModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestHttpInterceptor,
      multi: true,
      },
     
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
