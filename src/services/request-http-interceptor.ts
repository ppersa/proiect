import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
  } from '@angular/common/http'
  import { Injectable } from '@angular/core'
  import { Router } from '@angular/router'
  import { Observable, throwError } from 'rxjs'
  import { catchError } from 'rxjs/operators'
import { environment } from 'src/environments/environment'
  
  import { AuthService } from './auth.service'
  
  @Injectable()
  export class RequestHttpInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const jwt = this.authService.getToken()
      const request = req.clone(
        { setHeaders: 
          { authorization: `Bearer ${jwt}`},
          url: `${environment.proxyUrl}${req.url}`
        });
      return next.handle(request).pipe(
        catchError((err) => {
          return throwError(err)
        })
      )
    }
  }