import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchResult } from 'src/entity/search-result';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getSearchResults(name: string) {
    return this.http.get<any>(`${environment.apiUrl}search/series?name=${name}`);
  }

  getSeriesDetails(id: number) {
    return this.http.get<any>(`${environment.apiUrl}series/${id}`);
  }

  getEpisodes(id: number) {
    return this.http.get<any>(`${environment.apiUrl}series/${id}/episodes`);
  }

  getEpisodesBySeason(id: number, season: number) {
    return this.http.get<any>(`${environment.apiUrl}series/${id}/episodes/query?airedSeason=${season}`);
  }
}
