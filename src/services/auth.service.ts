import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private storageKey = 'token';

  constructor(private http: HttpClient) { }

  setToken(token: string) {
    localStorage.setItem(this.storageKey, token);
  }

  getToken() {
    return localStorage.getItem(this.storageKey);
  }

  init() {
    this.http.post<any>(
      `${environment.apiUrl}login`, 
      { apiKey: `${environment.apiKey}` }
    ).subscribe(data => {
      this.setToken(data.token);
    });
  }
}
