# -*- coding: utf-8 -*-


import flask
import requests
from flask_cors import CORS
from flask import request

app = flask.Flask(__name__)
CORS(app)

method_requests_mapping = {
    'GET': requests.get,
    'HEAD': requests.head,
    'POST': requests.post,
    'PUT': requests.put,
    'DELETE': requests.delete,
    'PATCH': requests.patch,
}


@app.route('/<path:url>', methods=method_requests_mapping.keys())
def proxy(url):
    print(flask.request)
    requests_function = method_requests_mapping[flask.request.method]
    request_headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MTI2NDA4MzksImlkIjoiU3Vic2NyaWJlciIsIm9yaWdfaWF0IjoxNjEyMDM2MDM5fQ.v4b9NGiFw1WQA0BmB3Rnw0qw8pIKbPSYkNIn1fbuNwgW9F7pNfEtWkShCQGCmYacDDzhe63AWs3bi7WavbR4q_zUUhUG3rYQoCC-fNnM1xPdbHjYa0DTuLltOGGdCt4eVQdI0gWTWRfre5nNe1Lco4Y9rj76CR9PZNo0mrApNTdAvQSm3V44fABjcVaCV4axMG3eigd_puuRhSstydUV2FOS9XOyp7UbsOjc3JU6uZK3JQmDlG15LEb7p7Aj5ih6REsYy_gByDXzUZr9hS5iwfy01-JWQGjpOJ7GOAJ6sxdfkneIZ6ZRv0VvPqZLUosh3LNOXvznBvxYayACTcZ1qw'};
    #print(url);
    print(flask.request.method)
    request = requests_function(url, stream=True, params=flask.request.args, json=flask.request.json, headers = request_headers)
    #del request.headers['Content-Length']
    #print(request.headers)
    response = flask.Response(flask.stream_with_context(request.iter_content()),
                              content_type=request.headers['content-type'],
                              status=request.status_code)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


if __name__ == '__main__':
    app.debug = True
    app.run()
